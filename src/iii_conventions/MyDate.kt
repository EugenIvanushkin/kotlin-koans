package iii_conventions

data class MyDate(val year: Int, val month: Int, val dayOfMonth: Int)

operator fun MyDate.plus(b:MyDate)=MyDate(this.year+b.year,this.month+b.month,this.dayOfMonth+b.dayOfMonth)

operator fun MyDate.compareTo(date: MyDate): Int {
    if (this.year.compareTo(date.year) == 0) {
        if (this.month.compareTo(date.month) == 0) {
            if (this.dayOfMonth.compareTo(date.dayOfMonth) == 0) {
                return 0
            }
        } else {
            return this.month.compareTo(date.month)
        }
    }
    return this.year.compareTo(date.year)
}


operator fun MyDate.rangeTo(other: MyDate): DateRange = DateRange(this,other)

enum class TimeInterval {
    DAY,
    WEEK,
    YEAR
}

class DateRange(val start: MyDate, val endInclusive: MyDate):Iterable<MyDate>{
    override fun iterator(): Iterator {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
    companion object Iterator:kotlin.collections.Iterator<MyDate>{
        override fun next(): MyDate {
            if(this.hasNext()){

            }
            return MyDate(1,1,1)
        }

        override fun hasNext(): Boolean {
            return false;
        }
    }

}

operator fun DateRange.contains(other:MyDate):Boolean{
    return other.compareTo(start)in 0..1 && other.compareTo(endInclusive)in -1..0
}
